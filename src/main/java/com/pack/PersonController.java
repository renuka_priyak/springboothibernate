package com.pack;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PersonController {

	@Autowired
	PersonDao persondao;

	@RequestMapping("/")
	public ModelAndView welcome() {
		return new ModelAndView("/index");
	}

	@RequestMapping("add")
	public ModelAndView addPerson(@ModelAttribute("person") Person person) {
		return new ModelAndView("personform");
	}

	@RequestMapping("save")
	public ModelAndView savePerson(@ModelAttribute("person") Person person, HttpServletRequest request) {
		if (person.getId() == 0) {
			persondao.addPerson(person);
		} else {
			persondao.updatePerson(person);
		}

		List<Person> l = persondao.getAllPersons();
		return new ModelAndView("success", "listpersons", l);
	}

	@RequestMapping("view")
	public ModelAndView viewPerson(@ModelAttribute("person") Person person) {

		List<Person> l = persondao.getAllPersons();
		return new ModelAndView("success", "listpersons", l);
	}

	@RequestMapping("editEmployee")

	public ModelAndView editPerson(@ModelAttribute("person") Person person, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		Person p = persondao.getPersonById(id);
		return new ModelAndView("personform", "person", p);
	}

	@RequestMapping("deleteEmployee")
	public ModelAndView deletePerson(@ModelAttribute("person") Person person, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		Person p = persondao.getPersonById(id);
		persondao.deletePerson(p);
		List<Person> Employeelist = persondao.getAllPersons();
		return new ModelAndView("success", "listpersons", Employeelist);
	}
}
