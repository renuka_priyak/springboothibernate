package com.pack;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PersonDao {

	@Autowired
	SessionFactory sessionFactory;

	public void addPerson(Person person) {

		sessionFactory.getCurrentSession().save(person);
	}

	public List<Person> getAllPersons() {

		return (List<Person>) sessionFactory.getCurrentSession().createQuery("from Person").list();

	}

	public Person getPersonById(int id) {
		Person p = (Person) sessionFactory.getCurrentSession().get(Person.class, id);

		return p;
	}

	public void updatePerson(Person person) {

		sessionFactory.getCurrentSession().update(person);
	}

	public void deletePerson(Person person) {
		
		sessionFactory.getCurrentSession().delete(person);
		
	}
	

}
