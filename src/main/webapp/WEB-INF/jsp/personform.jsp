<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
.errors
{
color:red;
font-weight:bold;
}
</style>
</head>
<body>
<form:form method="post" action="save" modelAttribute="person">
 <div class="container">
        <div class="jumbotron">
<table>
 <form:hidden path="id"/>
<tr>
<td>Name</td>
<td><form:input path="name" id="name"/>
</td>
<tr>
<td>City</td>
<td><form:input path="city" id="city"/>
</td>
 <tr>
<td colspan="2"><input type="submit" class="btn btn-success" value="save"></td>
    </tr>
</table>
</div>
</div>
</form:form>
</body>
</html>