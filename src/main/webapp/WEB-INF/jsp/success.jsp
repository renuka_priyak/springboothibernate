<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
     <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table border="1" style="border-collapse:collapse">
<tr><th>ID</th><th>Name</th><th>City</th></tr>

<c:forEach var="listperson" items="${listpersons}">
<tr>
<td>
<c:out value="${listperson.id}"></c:out></td>
<td><c:out value="${listperson.name}"></c:out></td>
<td><c:out value="${listperson.city}"></c:out></td>
<td><a href="editEmployee?id=${listperson.id}">Edit</a></td>
<td><a href="deleteEmployee?id=${listperson.id}">Delete</a></td>
</tr>
</c:forEach>
</table>

</body>
</html>